def reverser(&input)
  words = input.call.split
  words.map(&:reverse).join(" ")
end

def adder(add_value=1, &base)
  add_value + base.call
end

def repeater(instances=1, &action)
  instances.times(&action)
end
